##[
  Extends std/times with Date type.

  Examples
  ========

  .. code-block:: nim
    import dates
    let d = getDate()
    assert $d == "2022-02-23"
    assert $initDate(0) == "1970-01-01"
    assert $initDate(1, mJan, 1970) == "1970-01-01"

]##

import std/times {.all.}

export times

type
  Date* = distinct int64  ## Date class

  Quarter* = enum  ## Quarter
    q1 = (1, "Q1"), q2 = "Q2", q3 = "Q3", q4 = "Q4"

  DateUnit* = enum  ## Date units
    Weeks, Months, Quarters, Years

  YearweekRange* = range[1..53] ## Weeks in year range

proc toDate*(t: Time): Date =
  ## Convert Time object to Date object.
  Date(t.toUnix div secondsInDay)

proc toTime*(d: Date): Time =
  ## Convert Date object to Time object.
  (d.int64 * secondsInDay).fromUnix

proc `$`*(d: Date): string =
  ## String representation of Date object.
  d.toTime.format("yyyy-MM-dd")

proc date*(monthday: MonthdayRange, month: Month, year: int): Date =
  ## Init new Date object.
  Date(toEpochDay(monthday, month, year))

proc initDate*(epoch: int64): Date =
  ## Init new Date object.
  Date(epoch)

proc initDate*(monthday: MonthdayRange, month: Month, year: int): Date =
  ## Init new Date object.
  Date(toEpochDay(monthday, month, year))

proc initDate*(t: Time): Date =
  ## Init new Date object.
  t.toDate

proc initDate*(dt: DateTime): Date =
  ## Init new Date object.
  dt.toTime.toDate

proc parseDate*(input: string, f: string = "yyyy-MM-dd"): Date =
  ## Parse date from string.
  input.parseTime(f, utc()).toDate

proc year*(d: Date): int =
  ## Get year.
  fromEpochDay(d.int64).year

proc month*(d: Date): Month =
  ## Get month.
  fromEpochDay(d.int64).month

proc getQuarter*(m: Month): Quarter =
  Quarter((m.int - 1) div 3 + 1)

proc quarter*(d: Date): Quarter =
  ## Get quarter.
  getQuarter(d.month)

proc weekday*(d: Date): WeekDay =
  ## Get day of week.
  WeekDay((3 + d.int64) mod 7)

proc monthday*(d: Date): MonthdayRange =
  ## Get day of month.
  fromEpochDay(d.int64).monthday

proc yearday*(d: Date): YeardayRange =
  ## Get day of year.
  let (mday, month, year) = fromEpochDay(d.int64)
  getDayOfYear(mday, month, year)

proc week*(d: Date): YearweekRange =
  ## Get year week.
  (d.yearday + 1) div 7 + 1

proc isoweek*(d: Date): YearweekRange =
  ## Get ISO week number.
  let nearestThursday = 7 * ((d.int64 + 3) div 7)
  let startYear = nearestThursday - Date(nearestThursday).yearday
  int(1 + (nearestThursday - startYear) div 7)

proc quarterday*(d: Date): int =
  ## Get day of quarter.
  const quarterDays: array[Quarter, int] = [0, 90, 181, 273]
  let q = d.quarter
  result = d.yearday
  result -= quarterDays[q]
  if q > q1 and d.year.isLeapYear():
    result -= 1

proc isLeapDay*(d: Date): bool =
  ## Returns whether `t` is a leap day.
  let (mday, month, year) = fromEpochDay(d.int64)
  year.isLeapYear and month == mFeb and mday == 29

proc toDateTime*(d: Date, zone: Timezone = local()): DateTime =
  ## Convert Date object to DateTime object.
  let (mday, month, year) = fromEpochDay(d.int64)
  dateTime(year = year, month = month, monthday = mday, zone = zone)

proc getDate*(): Date =
  ## Get current date.
  getTime().toDate()

proc getDaysInMonth*(d: Date): int =
  ## Get the number of days in month.
  let (_, month, year) = fromEpochDay(d.int64)
  getDaysInMonth(month, year)

proc daysInQuarter*(quarter: Quarter, year: int): int =
  ## Get the number of days in quarter.
  case quarter
  of q1: result = if year.isLeapYear(): 91 else: 90
  of q2: result = 91
  of q3, q4: result = 92

proc getDaysInYear*(d: Date): int =
  ## Get the number of days in year.
  getDaysInYear(d.year)

proc `==`*(a, b: Date): bool =
  a.int64 == b.int64

template `!=`*(a, b: Date): bool =
  not (a == b)

proc `>`*(a, b: Date): bool =
  a.int64 > b.int64

proc `>=`*(a, b: Date): bool =
  a.int64 >= b.int64

template `<`*(a, b: Date): bool =
  not (a.int64 >= b.int64)

template `<=`*(a, b: Date): bool =
  not (a.int64 > b.int64)

proc `+`*(a, b: Date): int64 =
  a.int64 + b.int64

proc `-`*(a, b: Date): int64 =
  a.int64 - b.int64

proc `+`*(dt: Date, days: int64): Date =
  initDate(dt.int64 + days)

proc `-`*(dt: Date, days: int64): Date =
  initDate(dt.int64 - days)

proc `+=`*(a: var Date, b: int64) =
  a = a + b

proc `-=`*(a: var Date, b: int64) =
  a = a - b

proc `+`*(dt: Date, dur: Duration): Date =
  dt + dur.inDays

proc `-`*(dt: Date, dur: Duration): Date =
  result = dt - dur.inDays
  if (dur - initDuration(days = dur.inDays)).inNanoseconds > 0: result -= 1

proc `+=`*(a: var Date, b: Duration) =
  a = a + b

proc `-=`*(a: var Date, b: Duration) =
  a = a - b

proc floor*(d: Date, unit: DateUnit): Date =
  case unit:
    of Weeks:
      return d - ord(d.weekday)
    of Months:
      return d - ord(d.monthday)
    of Quarters:
      return d - d.quarterday
    of Years:
      return d - d.yearday

proc ceil*(d: Date, unit: DateUnit): Date =
  case unit:
    of Weeks:
      return d + (7 - ord(d.weekday))
    of Months:
      let (mday, month, year) = fromEpochDay(d.int64)
      return d + (getDaysInMonth(month, year) - ord(mday)) + 1
    of Quarters:
      return d + (daysInQuarter(d.quarter, d.year) - d.quarterday)
    of Years:
      return d + (getDaysInYear(d.year) - d.yearday)
