# Package

version       = "0.1.0"
author        = "Artem Klevtsov"
description   = "Provides classes and methods for dates (extends std/times)"
license       = "MIT"
srcDir        = "src"


# Dependencies

requires "nim >= 1.4.6"
