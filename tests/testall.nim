import std/unittest
import dates

const secondsInDay = 60 * 60 * 24
const d0 = initDate(0)
const d1 = initDate(18760)
const d2 = initDate(18321)
const d3 = initDate(29220)
const d4 = initDate(-25567)


suite "Init":

  test "iniDate from int64":
    check initDate(0).int64 == 0

  test "initDate from date parts":
    check initDate(-25567) == initDate(1,  mJan, 1900)
    check initDate(0)      == initDate(1,  mJan, 1970)
    check initDate(18760)  == initDate(13, mMay, 2021)
    check initDate(18321)  == initDate(29, mFeb, 2020)
    check initDate(29220)  == initDate(1,  mJan, 2050)


suite "Converts":

  test "convert to int64":
    check initDate(1,  mJan, 1900).int64 == -25567
    check initDate(1,  mJan, 1970).int64 == 0
    check initDate(13, mMay, 2021).int64 == 18760
    check initDate(29, mFeb, 2020).int64 == 18321
    check initDate(1,  mJan, 2050).int64 == 29220

  test "convert to Time":
    check initDate(-25567).toTime == initTime(-25567 * secondsInDay, 0)
    check initDate(0).toTime      == initTime(0 * secondsInDay, 0)
    check initDate(18760).toTime  == initTime(18760 * secondsInDay, 0)
    check initDate(18321).toTime  == initTime(18321 * secondsInDay, 0)
    check initDate(29220).toTime  == initTime(29220 * secondsInDay, 0)


  test "convert to DateTime":
    check initDate(1,  mJan, 1900).toDateTime(utc()) == dateTime(1900, mJan, 1, zone = utc())
    check initDate(1,  mJan, 1970).toDateTime(utc()) == dateTime(1970, mJan, 1, zone = utc())
    check initDate(13, mMay, 2021).toDateTime(utc()) == dateTime(2021, mMay, 13, zone = utc())
    check initDate(29, mFeb, 2020).toDateTime(utc()) == dateTime(2020, mFeb, 29, zone = utc())
    check initDate(1,  mJan, 2050).toDateTime(utc()) == dateTime(2050, mJan, 1, zone = utc())


suite "Parts":

  test "year":
    check initDate(1,  mJan, 1900).year == 1900
    check initDate(1,  mJan, 1970).year == 1970
    check initDate(13, mMay, 2021).year == 2021
    check initDate(29, mFeb, 2020).year == 2020
    check initDate(1,  mJan, 2050).year == 2050

  test "day of year":
    check initDate(1,  mJan, 1900).yearday == 0
    check initDate(1,  mJan, 1970).yearday == 0
    check initDate(13, mMay, 2021).yearday == 132
    check initDate(29, mFeb, 2020).yearday == 59
    check initDate(1,  mJan, 2050).yearday == 0

  test "day of month":
    check initDate(1,  mJan, 1900).monthday == 1
    check initDate(1,  mJan, 1970).monthday == 1
    check initDate(13, mMay, 2021).monthday == 13
    check initDate(29, mFeb, 2020).monthday == 29
    check initDate(1,  mJan, 2050).monthday == 1

  test "day of week":
    check initDate(1,  mJan, 1900).weekday == dMon
    check initDate(1,  mJan, 1970).weekday == dThu
    check initDate(13, mMay, 2021).weekday == dThu
    check initDate(29, mFeb, 2020).weekday == dSat
    check initDate(1,  mJan, 2050).weekday == dSat

  test "month":
    check initDate(1,  mJan, 1900).month == mJan
    check initDate(1,  mJan, 1970).month == mJan
    check initDate(13, mMay, 2021).month == mMay
    check initDate(29, mFeb, 2020).month == mFeb
    check initDate(1,  mJan, 2050).month == mJan

  test "week":
    check initDate(1,  mJan, 1900).week == 1
    check initDate(1,  mJan, 1970).week == 1
    check initDate(6,  mJan, 1970).week == 1
    check initDate(7,  mJan, 1970).week == 2
    check initDate(29, mFeb, 2020).week == 9
    check initDate(31, mDec, 2012).week == 53
    check initDate(31, mDec, 2021).week == 53

  test "isoweek":
    check initDate(1,  mJan, 1900).isoweek == 1
    check initDate(1,  mJan, 1970).isoweek == 1
    check initDate(4,  mJan, 1970).isoweek == 1
    check initDate(5,  mJan, 1970).isoweek == 2
    check initDate(7,  mJan, 1970).isoweek == 2
    check initDate(29, mFeb, 2020).isoweek == 9
    check initDate(31, mDec, 2012).isoweek == 1
    check initDate(31, mDec, 2021).isoweek == 52

  test "quarter":
    check initDate(1,  mJan, 1900).quarter == q1
    check initDate(29, mFeb, 2020).quarter == q1
    check initDate(1,  mMay, 2050).quarter == q2
    check initDate(1,  mJul, 2050).quarter == q3
    check initDate(15, mAug, 1970).quarter == q3
    check initDate(15, mSep, 1970).quarter == q3
    check initDate(1,  mDec, 2012).quarter == q4


suite "Is leap":

  test "leap day":
    check initDate(1,  mJan, 1970).isLeapDay == false
    check initDate(13, mMay, 2021).isLeapDay == false
    check initDate(29, mFeb, 2020).isLeapDay == true

  test "leap year":
    check initDate(1,  mJan, 1900).year.isLeapYear == false
    check initDate(1,  mJan, 1970).year.isLeapYear == false
    check initDate(29, mFeb, 2020).year.isLeapYear == true
    check initDate(1,  mJan, 2012).year.isLeapYear == true


suite "Compare ops":

  test "dates compare":
    check initDate(0) == initDate(0)
    check initDate(0) != initDate(1)
    check initDate(0) >= initDate(0)
    check initDate(1) >= initDate(0)
    check initDate(1)  > initDate(0)
    check initDate(0) <= initDate(0)
    check initDate(0) <= initDate(1)
    check initDate(0)  < initDate(1)


suite "Arith ops":

  test "dates ops":
    check (initDate(0) - initDate(0)) == 0
    check (initDate(7) - initDate(4)) == 3
    check initDate(0) + 2 == initDate(2)
    check initDate(0) - 1 == initDate(-1)
    check initDate(0) + initDuration(hours = 1) == initDate(0)
    check initDate(0) - initDuration(hours = 1) == initDate(-1)
    check initDate(0) + initDuration(days = 2) == initDate(2)
    check initDate(0) - initDuration(days = 1) == initDate(-1)


suite "toString":

  test "toString":
    check $initDate(1,  mJan, 1900) == "1900-01-01"
    check $initDate(1,  mJan, 1970) == "1970-01-01"
    check $initDate(13, mMay, 2021) == "2021-05-13"
    check $initDate(29, mFeb, 2020) == "2020-02-29"
    check $initDate(1,  mJan, 2050) == "2050-01-01"

  test "getDate":
    check $getDate() == CompileDate
